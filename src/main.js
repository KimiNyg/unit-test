// src/main.js

const express = require('express')
const mylib = require('./mylib')

const app = express()
const port = 3000;

console.log({
    sum: mylib.sum(1,1), 
    random: mylib.random(), 
    arrGen: mylib.arrayGen()
})

app.get('/add', (req,res)=>{
    const a = parseInt(req.query.a)
    const b = parseInt(req.query.b)
    res.send(`${mylib.sum(a,b)}`)
})

app.get('/',(req,res)=>{
    res.send('Hello world!')
})

app.listen(port, ()=> {
    console.log(`Server: http://localhost:${port}`)
})